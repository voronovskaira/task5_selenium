package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailHomePagePO extends BasePage {
    public GmailHomePagePO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='z0']/*[@role='button']")
    private WebElement writeAMessageButton;

    @FindBy(name = "to")
    private WebElement writeAMessageToLine;

    @FindBy(name = "subjectbox")
    private WebElement subjectLine;

    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private WebElement writeAMessageBox;

    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private WebElement sendAMessageButton;

    @FindBy(id = "link_vsm")
    private WebElement openSentMessage;

    @FindBy(xpath = "//div[@style='display:']")
    private WebElement messageDetails;


    public void clickOnWriteAMessageButton() {
        waitElementToBeClickable(writeAMessageButton);
        writeAMessageButton.click();
    }

    public void writeAMessageTo(String email) {
        writeAMessageToLine.sendKeys(email);
    }

    public void writeASubjectOfMessage(String subject) {
        subjectLine.sendKeys(subject);
    }

    public void writeAMessage(String message) {
        writeAMessageBox.sendKeys(message);
    }

    public void sendAMessage() {
        sendAMessageButton.click();
    }

    public void openSentMessage() {
        openSentMessage.click();
    }

    public String getMessageDetails() {
        return messageDetails.getText();
    }
}
