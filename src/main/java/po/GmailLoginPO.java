package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPO extends BasePage {
    @FindBy(xpath = "//*[@id='identifierId']")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id='identifierNext']/span/span")
    private WebElement submitLogin;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(id = "passwordNext")
    private WebElement passwordNextButton;

    public GmailLoginPO(WebDriver driver) {
        super(driver);
    }

    public void inputLoginAndClick(String login) {
        this.loginInput.sendKeys(login);
        this.submitLogin.click();
    }

    public void inputPasswordAndClick(String password) {
        this.passwordInput.sendKeys(password);
        new Actions(driver).clickAndHold(passwordNextButton).release().perform();
    }
}
