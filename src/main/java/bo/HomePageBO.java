package bo;

import driver.DriverManager;
import po.AlertPO;
import po.GmailHomePagePO;

public class HomePageBO {
    private GmailHomePagePO gmailHomePagePO;
    private AlertPO alertPO;

    public HomePageBO() {
        this.gmailHomePagePO = new GmailHomePagePO(DriverManager.getDriver());
        this.alertPO = new AlertPO(DriverManager.getDriver());
    }

    public void sendMessage(String email, String subject, String text) {
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(email);
        gmailHomePagePO.writeASubjectOfMessage(subject);
        gmailHomePagePO.writeAMessage(text);
        gmailHomePagePO.sendAMessage();
    }

    public boolean verifyAlertIsPresent(String invalidEmail) {
        return alertPO.getAlertMessage().contains(invalidEmail);
    }
    public void closeAlert(){
        alertPO.clickOkOnAlertPopup();
        alertPO.clickCloseOnAlertPopup();
    }
    public boolean verifyMessageSentSuccessfully(String message){
        gmailHomePagePO.openSentMessage();
        return gmailHomePagePO.getMessageDetails().contains(message);
    }
}
